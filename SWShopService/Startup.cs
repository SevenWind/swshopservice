﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Extensions.Logging;
using SWShopService.Models;

namespace SWShopService {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddEntityFrameworkNpgsql().AddDbContext<SWShopDbContext>()
               .BuildServiceProvider();

            var authConfig = new AuthOptions();
            Configuration.GetSection("AuthOptions").Bind(authConfig);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options => {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters {

                            ValidateIssuer = true,
                            ValidIssuer = authConfig.ISSUER,
                            ValidateAudience = true,
                            ValidAudience = authConfig.AUDIENCE,
                            ValidateLifetime = true,
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            ValidateIssuerSigningKey = true,
                        };
                    });

            services.AddCors(options => {
                options.AddPolicy("AllowLocalHost",
                    builder => builder.WithOrigins(Configuration.GetValue<string>("AllowedHosts")).AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseHsts();
            }

            GlobalDiagnosticsContext.Set("configDir", AppDomain.CurrentDomain.BaseDirectory);
            GlobalDiagnosticsContext.Set("connectionString", Configuration.GetConnectionString("DefaultConnection"));

            loggerFactory.AddNLog();

            var serviceProvider = new ServiceCollection()
                                    .AddSingleton(loggerFactory)
                                    .AddLogging();

            app.UseCors("AllowLocalHost");

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
