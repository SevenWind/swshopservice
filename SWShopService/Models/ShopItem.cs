﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SWShopService.Models {
    public class ShopItem {
        [DatabaseGenerated (DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid id { get; set; }
        public int count { get; set; }
        public int buyGoldCost { get; set; }
        public int buyCoinCost { get; set; }
        public int sellGoldCost { get; set; }
        public int sellCoinsCost { get; set; }

        public bool isAvail { get; set; }
        public bool isDeleted { get; set; }
        public int limit { get; set; } = -1;
        public Guid item_id { get; set; }

        [ForeignKey ("item_id")]
        public Item item { get; set; }
    }
}