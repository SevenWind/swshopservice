﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SWShopService.Models {
    public class Item {
        [DatabaseGenerated (DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string image { get; set; }
        public bool isDeleted { get; set; }
        public int characterId { get; set; }
        public int rarity_id { get; set; }

        [ForeignKey ("rarity_id")]
        public Rarity rarity { get; set; }
    }
}