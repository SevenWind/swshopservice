﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWShopService.Models {
    public class Enums {
        public enum ItemCode {
            Coins = 1,
            Gold,
            ScrollOfHiring,
            MagicAmulet,
            GekkelKilnFragment,
            DornKirpichFragment,
            CerberFragment,
            TorilBuiniyFragment,
            LiaAlleriaFragment,
            HemingRouterFragment,
        }

        public static class AuthRole {
            public const string ADMIN = "Admin";
            public const string PLAYER = "Player";
            public const string TESTER = "Tester";
            public const string ANY = "Admin, Player, Tester";
        }
    }
}