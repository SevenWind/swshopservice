﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SWShopService.Models {
    public class Rarity {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
    }
}
