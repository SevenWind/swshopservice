namespace SWShopService.Models {
    public class ShopFilter {
        public string name { get; set; }
        public int isAvail { get; set; }
        public int nameSort { get; set; }
        public int buyGoldCost { get; set; }
        public int buyCoinsCost { get; set; }
        public int sellGoldCost { get; set; }
        public int sellCoinsCost { get; set; }
    }
}