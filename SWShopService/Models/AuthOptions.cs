﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWShopService.Models {
    public class AuthOptions {
        public string ISSUER { get; set; }
        public string AUDIENCE { get; set; }
        public int LIFETIME { get; set; }
        public string KEY { get => secureKEY; set => secureKEY = value; }
        /// <summary>
        /// Ключ шифрования
        /// </summary>
        private static string secureKEY = "!HD!d11!aAfr8fFEGD7A7s";

        public static SymmetricSecurityKey GetSymmetricSecurityKey() {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secureKEY));
        }
    }
}
