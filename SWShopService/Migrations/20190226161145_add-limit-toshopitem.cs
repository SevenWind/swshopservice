﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWShopService.Migrations
{
    public partial class addlimittoshopitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "limit",
                table: "ShopItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("977ee24f-7d18-49d6-b122-52405273bf66"),
                column: "limit",
                value: -1);

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("b2004ede-5bed-490a-9e04-cb15c0e4d2bd"),
                column: "limit",
                value: -1);

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("cd770f15-e6ab-4cb3-93b5-a048be0b6b11"),
                column: "limit",
                value: -1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "limit",
                table: "ShopItems");
        }
    }
}
