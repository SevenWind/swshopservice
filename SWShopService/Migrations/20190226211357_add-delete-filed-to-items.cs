﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWShopService.Migrations
{
    public partial class adddeletefiledtoitems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ShopItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Items",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("b2004ede-5bed-490a-9e04-cb15c0e4d2bd"),
                column: "isAvail",
                value: false);

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("cd770f15-e6ab-4cb3-93b5-a048be0b6b11"),
                column: "isAvail",
                value: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ShopItems");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Items");

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("b2004ede-5bed-490a-9e04-cb15c0e4d2bd"),
                column: "isAvail",
                value: true);

            migrationBuilder.UpdateData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("cd770f15-e6ab-4cb3-93b5-a048be0b6b11"),
                column: "isAvail",
                value: true);
        }
    }
}
