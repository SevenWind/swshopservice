﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SWShopService.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Rarity",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rarity", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    code = table.Column<string>(nullable: true),
                    image = table.Column<string>(nullable: true),
                    rarity_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.id);
                    table.ForeignKey(
                        name: "FK_Items_Rarity_rarity_id",
                        column: x => x.rarity_id,
                        principalTable: "Rarity",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShopItems",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    count = table.Column<int>(nullable: false),
                    buyGoldCost = table.Column<int>(nullable: false),
                    buyCoinCost = table.Column<int>(nullable: false),
                    sellGoldCost = table.Column<int>(nullable: false),
                    sellCoinCost = table.Column<int>(nullable: false),
                    isAvail = table.Column<bool>(nullable: false),
                    item_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShopItems", x => x.id);
                    table.ForeignKey(
                        name: "FK_ShopItems_Items_item_id",
                        column: x => x.item_id,
                        principalTable: "Items",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Rarity",
                columns: new[] { "id", "code" },
                values: new object[,]
                {
                    { 1, "None" },
                    { 2, "COMMON" },
                    { 3, "UNCOMMON" },
                    { 4, "RARE" }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "id", "code", "image", "name", "rarity_id" },
                values: new object[,]
                {
                    { new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), "ScrollOfHiring", "items/scroll.png", "Свиток Найма", 1 },
                    { new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), "MagicAmulet", "items/amulet.png", "Магический амулет", 1 },
                    { new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), "Coins", "items/coin.png", "Монеты", 2 },
                    { new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"), "Gold", "items/gold.png", "Золото", 4 }
                });

            migrationBuilder.InsertData(
                table: "ShopItems",
                columns: new[] { "id", "buyCoinCost", "buyGoldCost", "count", "isAvail", "item_id", "sellCoinCost", "sellGoldCost" },
                values: new object[] { new Guid("b2004ede-5bed-490a-9e04-cb15c0e4d2bd"), 50000, 100, 1, true, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), 35000, 75 });

            migrationBuilder.InsertData(
                table: "ShopItems",
                columns: new[] { "id", "buyCoinCost", "buyGoldCost", "count", "isAvail", "item_id", "sellCoinCost", "sellGoldCost" },
                values: new object[] { new Guid("cd770f15-e6ab-4cb3-93b5-a048be0b6b11"), 75000, 125, 1, true, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), 50000, 100 });

            migrationBuilder.InsertData(
                table: "ShopItems",
                columns: new[] { "id", "buyCoinCost", "buyGoldCost", "count", "isAvail", "item_id", "sellCoinCost", "sellGoldCost" },
                values: new object[] { new Guid("977ee24f-7d18-49d6-b122-52405273bf66"), 0, 100, 50000, true, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), 0, 0 });

            migrationBuilder.CreateIndex(
                name: "IX_Items_rarity_id",
                table: "Items",
                column: "rarity_id");

            migrationBuilder.CreateIndex(
                name: "IX_ShopItems_item_id",
                table: "ShopItems",
                column: "item_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShopItems");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Rarity");
        }
    }
}
