﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SWShopService.Migrations
{
    public partial class shopitemchanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "sellCoinCost",
                table: "ShopItems",
                newName: "sellCoinsCost");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "sellCoinsCost",
                table: "ShopItems",
                newName: "sellCoinCost");
        }
    }
}
