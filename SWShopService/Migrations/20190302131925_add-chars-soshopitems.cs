﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWShopService.Migrations
{
    public partial class addcharssoshopitems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ShopItems",
                columns: new[] { "id", "buyCoinCost", "buyGoldCost", "count", "isAvail", "isDeleted", "item_id", "limit", "sellCoinsCost", "sellGoldCost" },
                values: new object[,]
                {
                    { new Guid("38decd85-f6d9-4d8f-9ce3-8a36bb1a4ad7"), 50000, 0, 100, false, false, new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"), -1, 0, 0 },
                    { new Guid("d8cf2ae9-8d22-4d2b-bb89-19fe5b382929"), 50000, 0, 100, false, false, new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"), -1, 0, 0 },
                    { new Guid("ed18f5cf-760e-4c4f-bac1-53f96e3daf8e"), 50000, 100, 1, false, false, new Guid("c4993306-2a61-4302-84a0-5825d50d1ae8"), -1, 0, 0 },
                    { new Guid("6961aa0b-dcc1-4bed-a4a2-dd62db12a4f9"), 50000, 100, 1, false, false, new Guid("10b34f8b-defe-44f4-9dbe-3c7e375b2b1a"), -1, 0, 0 },
                    { new Guid("f48f0751-53e5-4eda-bec2-e867d4c943fc"), 50000, 100, 1, false, false, new Guid("7dd83f78-ad3d-49c6-a284-d5c9c68bfe01"), -1, 0, 0 },
                    { new Guid("945def88-2af6-41da-8340-c3de3135838f"), 100000, 200, 1, false, false, new Guid("75be5201-fa5c-40b5-9e81-a5f5a09311ac"), -1, 0, 0 },
                    { new Guid("3ee92d3d-9cad-4f96-b101-d5398fd572e8"), 100000, 200, 1, false, false, new Guid("e2e0c07e-e82d-4c72-8dbe-abb0ec02b86f"), -1, 0, 0 },
                    { new Guid("f1238cb6-49ab-4eac-8a16-9064a653440a"), 100000, 200, 1, false, false, new Guid("055f174a-3963-43c3-8a86-bf043b3e22d8"), -1, 0, 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("38decd85-f6d9-4d8f-9ce3-8a36bb1a4ad7"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("3ee92d3d-9cad-4f96-b101-d5398fd572e8"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("6961aa0b-dcc1-4bed-a4a2-dd62db12a4f9"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("945def88-2af6-41da-8340-c3de3135838f"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("d8cf2ae9-8d22-4d2b-bb89-19fe5b382929"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("ed18f5cf-760e-4c4f-bac1-53f96e3daf8e"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("f1238cb6-49ab-4eac-8a16-9064a653440a"));

            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("f48f0751-53e5-4eda-bec2-e867d4c943fc"));
        }
    }
}
