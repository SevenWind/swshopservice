﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWShopService.Migrations
{
    public partial class littleitemfixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ShopItems",
                keyColumn: "id",
                keyValue: new Guid("d8cf2ae9-8d22-4d2b-bb89-19fe5b382929"));

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"),
                column: "image",
                value: "../../../assets/items/scroll.png");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"),
                column: "image",
                value: "../../../assets/items/gold.png");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"),
                column: "image",
                value: "../../../assets/items/amulet.png");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"),
                column: "image",
                value: "../../../assets/items/coin.png");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"),
                column: "image",
                value: "items/scroll.png");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"),
                column: "image",
                value: "items/gold.png");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"),
                column: "image",
                value: "items/amulet.png");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"),
                column: "image",
                value: "items/coin.png");

            migrationBuilder.InsertData(
                table: "ShopItems",
                columns: new[] { "id", "buyCoinCost", "buyGoldCost", "count", "isAvail", "isDeleted", "item_id", "limit", "sellCoinsCost", "sellGoldCost" },
                values: new object[] { new Guid("d8cf2ae9-8d22-4d2b-bb89-19fe5b382929"), 50000, 0, 100, false, false, new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"), -1, 0, 0 });
        }
    }
}
