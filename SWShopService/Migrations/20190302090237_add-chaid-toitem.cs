﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SWShopService.Migrations
{
    public partial class addchaidtoitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "characterId",
                table: "Items",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "characterId",
                table: "Items");
        }
    }
}
