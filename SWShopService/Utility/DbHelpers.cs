using System.Linq;
using SWShopService.Models;

namespace SWShopService.Utility {
    public static class DbHelper {
        /// <summary>
        /// Сортировка предметов
        /// </summary>
        /// <param name="items">предметы</param>
        /// <param name="filter">фильтр</param>
        public static IQueryable<ShopItem> OrderItems (IQueryable<ShopItem> items, ShopFilter filter) {
            if (filter.nameSort != 0) {
                if (filter.nameSort == 1) {
                    return items.OrderBy (a => a.item.name);
                } else {
                    return items.OrderByDescending (a => a.item.name);
                }
            }
            if (filter.buyCoinsCost != 0) {
                if (filter.buyCoinsCost == 1) {
                    return items.OrderBy (a => a.buyCoinCost);
                } else {
                    return items.OrderByDescending (a => a.buyCoinCost);
                }
            }
            if (filter.buyGoldCost != 0) {
                if (filter.buyGoldCost == 1) {
                    return items.OrderBy (a => a.buyGoldCost);
                } else {
                    return items.OrderByDescending (a => a.buyGoldCost);
                }
            }
            if (filter.sellCoinsCost != 0) {
                if (filter.sellCoinsCost == 1) {
                    return items.OrderBy (a => a.sellCoinsCost);
                } else {
                    return items.OrderByDescending (a => a.sellCoinsCost);
                }
            }
            if (filter.sellGoldCost != 0) {
                if (filter.sellGoldCost == 1) {
                    return items.OrderBy (a => a.sellGoldCost);
                } else {
                    return items.OrderByDescending (a => a.sellGoldCost);
                }
            }
            return items;
        }
    }
}