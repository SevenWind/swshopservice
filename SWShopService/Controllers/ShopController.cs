﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWShopService.Models;
using SWShopService.Utility;
using static SWShopService.Models.Enums;

namespace SWShopService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ShopController : ControllerBase {

        private readonly SWShopDbContext db;
        private readonly ILogger<ShopController> logger;

        public ShopController (SWShopDbContext _db, ILogger<ShopController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        [HttpPost ("pageNumber={pageNumber}&pageSize={pageSize}")]
        public IActionResult GetShopItems (int pageNumber, int pageSize, [FromBody] ShopFilter filter) {
            try {
                var fullShopItems = db.ShopItems.Include (a => a.item).Include (a => a.item.rarity)
                    .Where (a => a.isDeleted == false && a.isAvail);

                if (!string.IsNullOrWhiteSpace (filter.name) && filter.name != "_") {
                    fullShopItems = fullShopItems.Where (a => a.item.name.ToUpper ().StartsWith (filter.name.ToUpper ()));
                }

                if (filter.isAvail != 0) {
                    if (filter.isAvail == 1) {
                        fullShopItems = fullShopItems.Where (a => a.isAvail);
                    } else {
                        fullShopItems = fullShopItems.Where (a => !a.isAvail);
                    }
                }

                fullShopItems = DbHelper.OrderItems (fullShopItems, filter);

                var shopItems = fullShopItems
                    .Skip ((pageNumber - 1) * pageSize)
                    .Take (pageSize)
                    .ToArray ();
                return Ok (new {
                    shopItems,
                    pageCount = (int) Math.Ceiling ((double) fullShopItems.Count () / pageSize),
                    pageNumber
                });
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("pageNumber={pageNumber}&pageSize={pageSize}")]
        public IActionResult GetAllShopItems (int pageNumber, int pageSize, [FromBody] ShopFilter filter) {
            try {
                var fullShopItems = db.ShopItems.Include (a => a.item).Include (a => a.item.rarity)
                    .Where (a => a.isDeleted == false);

                if (!string.IsNullOrWhiteSpace (filter.name) && filter.name != "_") {
                    fullShopItems = fullShopItems.Where (a => a.item.name.ToUpper ().StartsWith (filter.name.ToUpper ()));
                }

                if (filter.isAvail != 0) {
                    if (filter.isAvail == 1) {
                        fullShopItems = fullShopItems.Where (a => a.isAvail);
                    } else {
                        fullShopItems = fullShopItems.Where (a => !a.isAvail);
                    }
                }

                fullShopItems = DbHelper.OrderItems (fullShopItems, filter);

                var shopItems = fullShopItems
                    .Skip ((pageNumber - 1) * pageSize)
                    .Take (pageSize)
                    .ToArray ();
                return Ok (new {
                    shopItems,
                    pageCount = (int) Math.Ceiling ((double) fullShopItems.Count () / pageSize),
                    pageNumber
                });
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpGet ("pageNumber={pageNumber}&pageSize={pageSize}")]
        public IActionResult GetItems (int pageNumber, int pageSize) {
            try {
                var shopItems = db.Items.Include (a => a.rarity)
                    .Where (a => a.isDeleted == false)
                    .OrderByDescending (a => a.rarity.id)
                    .Skip ((pageNumber - 1) * pageSize).Take (pageSize).ToArray ();
                return Ok (new {
                    shopItems,
                    pageCount = (int) Math.Ceiling ((double) shopItems.Count () / pageSize),
                    pageNumber
                });
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpGet ("{itemId}")]
        public async Task<IActionResult> GetSellItemCost (Guid itemId) {
            try {
                var item = await db.ShopItems.Include (a => a.item).Include (a => a.item.rarity).Where (a => a.isDeleted == false).FirstOrDefaultAsync (a => a.item_id == itemId);
                if (item == null)
                    return NotFound ();
                return Ok (new {
                    gold = item.sellGoldCost / 2,
                        coins = item.sellCoinsCost / 2
                });
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}