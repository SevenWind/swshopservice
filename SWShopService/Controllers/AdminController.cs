﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWShopService.Models;
using static SWShopService.Models.Enums;

namespace SWShopService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class AdminController : ControllerBase {

        private readonly SWShopDbContext db;
        private readonly ILogger<AdminController> logger;

        public AdminController (SWShopDbContext _db, ILogger<AdminController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        [HttpPost ("itemId={itemId}&isLock={isLock}")]
        public async Task<IActionResult> ChangeLockItem (Guid itemId, bool isLock) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = await db.ShopItems.Include (a => a.item).FirstOrDefaultAsync (a => a.item_id == itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isAvail = isLock;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("{itemId}")]
        public async Task<IActionResult> DeleteItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var shopItem = await db.ShopItems.Include (a => a.item).FirstOrDefaultAsync (a => a.item_id == itemId);
                    if (shopItem == null) {
                        return NotFound ();
                    }
                    shopItem.isDeleted = true;
                    shopItem.item.isDeleted = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("{itemId}")]
        public async Task<IActionResult> RestoreItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var shopItem = await db.ShopItems.Include (a => a.item).FirstOrDefaultAsync (a => a.item_id == itemId);
                    if (shopItem == null) {
                        return NotFound ();
                    }
                    shopItem.isDeleted = false;
                    shopItem.item.isDeleted = false;

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveItem ([FromBody] ShopItem item) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var shopItem = await db.ShopItems.Include (a => a.item).FirstOrDefaultAsync (a => a.item_id == item.item_id);
                    if (shopItem == null) {
                        // create new
                        var dbItem = new Item ();
                        dbItem.name = item.item.name;
                        dbItem.code = item.item.code;
                        dbItem.image = item.item.image;
                        dbItem.rarity_id = item.item.rarity_id;
                        dbItem.characterId = item.item.characterId;
                        await db.Items.AddAsync (dbItem);
                        await db.SaveChangesAsync ();

                        shopItem = new ShopItem ();
                        shopItem.buyCoinCost = item.buyCoinCost;
                        shopItem.buyGoldCost = item.buyGoldCost;
                        shopItem.sellCoinsCost = item.sellCoinsCost;
                        shopItem.sellGoldCost = item.sellGoldCost;
                        shopItem.limit = item.limit;
                        shopItem.count = item.count;
                        shopItem.item_id = dbItem.id;
                        await db.ShopItems.AddAsync (shopItem);
                        await db.SaveChangesAsync ();
                        tr.Commit ();
                        return Ok (dbItem.id);
                    }
                    // update
                    shopItem.buyCoinCost = item.buyCoinCost;
                    shopItem.buyGoldCost = item.buyGoldCost;
                    shopItem.sellCoinsCost = item.sellCoinsCost;
                    shopItem.sellGoldCost = item.sellGoldCost;
                    shopItem.limit = item.limit;
                    shopItem.count = item.count;
                    shopItem.item.name = item.item.name;
                    shopItem.item.code = item.item.code;
                    if (!string.IsNullOrWhiteSpace (item.item.image) && item.item.image != shopItem.item.image)
                        shopItem.item.image = item.item.image;
                    shopItem.item.rarity_id = item.item.rarity_id;
                    shopItem.item.characterId = item.item.characterId;

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (shopItem.item.id);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}